package pl.mwalaszek.bugtracker.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mwalaszek.bugtracker.persistence.dao.UserRepository;
import pl.mwalaszek.bugtracker.persistence.model.User;

import java.util.List;

@Service
public class UserService {
    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    public User getByUsername(String userName){
        return userRepository.findByUsername(userName);
    }

    public User getById(Long id){
        return userRepository.findOne(id);
    }
}
