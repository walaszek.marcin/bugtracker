package pl.mwalaszek.bugtracker.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mwalaszek.bugtracker.persistence.dao.IssueRepository;
import pl.mwalaszek.bugtracker.persistence.dto.IssueDTO;
import pl.mwalaszek.bugtracker.persistence.model.STATE;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class IssueService {
    private IssueRepository issueRepository;
    private UserService userService;

    @Autowired
    public IssueService(IssueRepository issueRepository, UserService userService) {
        this.issueRepository = issueRepository;
        this.userService = userService;
    }

    public List<IssueDTO> getTodoList(){
        return issueRepository.findAll()
                .stream()
                .filter(i -> i.getState().equals(STATE.TODO))
                .map(issue -> new IssueDTO(issue, userService.getById(issue.getAssigneeId()).getUsername()))
                .collect(Collectors.toList());
    }

    public List<IssueDTO> getInDevList(){
        return issueRepository.findAll()
                .stream()
                .filter(i -> i.getState().equals(STATE.DEV))
                .map(issue -> new IssueDTO(issue, userService.getById(issue.getAssigneeId()).getUsername()))
                .collect(Collectors.toList());
    }

    public List<IssueDTO> getDoneList(){
        return issueRepository.findAll()
                .stream()
                .filter(i -> i.getState().equals(STATE.DONE))
                .map(issue -> new IssueDTO(issue, userService.getById(issue.getAssigneeId()).getUsername()))
                .collect(Collectors.toList());
    }

}
