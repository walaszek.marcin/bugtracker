package pl.mwalaszek.bugtracker.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.mwalaszek.bugtracker.service.IssueService;

@Controller
public class IndexController {

    private IssueService issueService;

    @Autowired
    public IndexController(IssueService issueService) {
        this.issueService = issueService;
    }

    @GetMapping("/index")
    public String getYourPage(Model model) {
        model.addAttribute("issueService", issueService);
        return "index";
    }
}