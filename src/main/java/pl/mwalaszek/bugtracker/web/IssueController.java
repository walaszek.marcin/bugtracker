package pl.mwalaszek.bugtracker.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.mwalaszek.bugtracker.persistence.dao.IssueRepository;
import pl.mwalaszek.bugtracker.persistence.model.Issue;
import pl.mwalaszek.bugtracker.persistence.model.STATE;
import pl.mwalaszek.bugtracker.service.UserService;

import java.security.Principal;

@Controller
public class IssueController {
    private IssueRepository issueRepository;
    private UserService userService;

    @Autowired
    public IssueController(IssueRepository issueRepository, UserService userService) {
        this.issueRepository = issueRepository;
        this.userService = userService;
    }

    @GetMapping("/addIssue")
    public String getYourPage(Model model) {
        model.addAttribute("issue", new Issue());
        model.addAttribute("users", userService.getAllUsers());
        return "addIssue";
    }

    @RequestMapping(value = "/saveIssue", method=RequestMethod.POST)
    public String saveIssue(@ModelAttribute(value = "issue") Issue issue) {
        issue.setState(STATE.TODO);
        issueRepository.save(issue);
        return "redirect:/index";
    }

    @RequestMapping(value = "/moveToDev/{id}", method=RequestMethod.POST)
    public String moveToDev(@PathVariable Long id) {
        Issue issue = issueRepository.findOne(id);
        issue.setState(STATE.DEV);
        issueRepository.save(issue);
        return "redirect:/index";
    }

    @RequestMapping(value = "/moveToDone/{id}", method=RequestMethod.POST)
    public String moveToDone(@PathVariable Long id) {
        Issue issue = issueRepository.findOne(id);
        issue.setState(STATE.DONE);
        issueRepository.save(issue);
        return "redirect:/index";
    }

    @RequestMapping(value = "/closeIssue/{id}", method=RequestMethod.POST)
    public String closeIssue(@PathVariable Long id) {
        Issue issue = issueRepository.findOne(id);
        issue.setState(STATE.CLOSED);
        issueRepository.save(issue);
        return "redirect:/index";
    }

    @RequestMapping(value = "/assignToMe/{id}", method=RequestMethod.POST)
    public String assignToMe(@PathVariable Long id, Principal principal) {
        Issue issue = issueRepository.findOne(id);
        issue.setAssigneeId(userService.getByUsername(principal.getName()).getId());
        issueRepository.save(issue);
        return "redirect:/index";
    }
}
