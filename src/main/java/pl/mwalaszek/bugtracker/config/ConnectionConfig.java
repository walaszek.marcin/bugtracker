package pl.mwalaszek.bugtracker.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.google.connect.GoogleConnectionFactory;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;

import javax.inject.Inject;

@Configuration
public class ConnectionConfig {

    @Inject
    private Environment environment;

    @Bean
    @Primary
    public ConnectionFactoryLocator customConnectionFactoryLocator() {
        ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();
        registry.addConnectionFactory(new TwitterConnectionFactory(
                environment.getProperty("twitter.consumerKey"),
                environment.getProperty("twitter.consumerSecret")));
        registry.addConnectionFactory(new GoogleConnectionFactory(
                environment.getProperty("google.consumerKey"),
                environment.getProperty("google.consumerSecret")));

        return registry;
    }
}
