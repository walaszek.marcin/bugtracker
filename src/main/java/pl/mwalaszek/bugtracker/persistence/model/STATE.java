package pl.mwalaszek.bugtracker.persistence.model;

public enum STATE {
    TODO, DEV, DONE, CLOSED;
}
