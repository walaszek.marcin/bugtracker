package pl.mwalaszek.bugtracker.persistence.dao;

import pl.mwalaszek.bugtracker.persistence.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(final String username);
}
