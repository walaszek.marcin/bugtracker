package pl.mwalaszek.bugtracker.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mwalaszek.bugtracker.persistence.model.Issue;
import pl.mwalaszek.bugtracker.persistence.model.User;

public interface IssueRepository extends JpaRepository<Issue, Long> {
}
