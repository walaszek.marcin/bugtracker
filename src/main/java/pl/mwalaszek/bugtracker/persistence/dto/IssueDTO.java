package pl.mwalaszek.bugtracker.persistence.dto;

import pl.mwalaszek.bugtracker.persistence.model.Issue;

public class IssueDTO {
    private Long id;
    private String name;
    private String description;
    private Integer estimate;
    private String assignee;

    public IssueDTO(Issue issue, String assignee) {
        this.id = issue.getId();
        this.name = issue.getName();
        this.description = issue.getDescription();
        this.estimate = issue.getEstimate();
        this.assignee = assignee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public Integer getEstimate() {
        return estimate;
    }

    public void setEstimate(Integer estimate) {
        this.estimate = estimate;
    }
}
