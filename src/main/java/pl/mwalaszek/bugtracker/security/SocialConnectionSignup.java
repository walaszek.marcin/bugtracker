package pl.mwalaszek.bugtracker.security;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import org.springframework.social.connect.UserProfile;
import pl.mwalaszek.bugtracker.persistence.dao.UserRepository;
import pl.mwalaszek.bugtracker.persistence.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.stereotype.Service;

@Service
public class SocialConnectionSignup implements ConnectionSignUp {

    @Autowired
    private UserRepository userRepository;

    @Override
    public String execute(Connection<?> connection) {
        UserProfile userProfile = connection.fetchUserProfile();
        User user;
        if (userProfile.getEmail() != null){
            user = new User(userProfile.getEmail(), connection.getDisplayName(), randomAlphabetic(8), connection.getKey().getProviderId());
        } else {
            user = new User("n/a", connection.getDisplayName(), randomAlphabetic(8), connection.getKey().getProviderId());
        }
        if (userRepository.findByUsername(user.getUsername()) == null) {
            userRepository.save(user);
        }
        return user.getUsername();
    }
}
